package com.example.android.myappkotlin

import android.content.Context
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.example.android.myappkotlin.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private lateinit var binding : ActivityMainBinding
    private val myName :MyName = MyName("Angela Rios")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        binding.myName = myName

        binding.btGuardar.setOnClickListener {
            addUsuario(it); //llamo a esta funcion iT es este
        }

        /**
        setContentView(activity_main)
        val clickBtGuardar= findViewById<Button>(R.id.bt_guardar)
        clickBtGuardar.setOnClickListener {
            addUsuario(it); //llamo a esta funcion iT es este
        }
        **/
    }

    private fun addUsuario(view:View){


        binding.apply {
            //nombreText.text = binding.editName.text
           myName?.usuario =   this.editName.text.toString()

            invalidateAll()
            editName.visibility = View.GONE
            view.visibility = View.GONE
            this.inNombre.visibility = View.VISIBLE

        }

        /**

        val editName = findViewById<EditText>(R.id.edit_name)
        val nombreText = findViewById<TextView>(R.id.in_nombre)

        nombreText.text = editName.text
        editName.visibility = View.GONE
        view.visibility = View.GONE
        nombreText.visibility = View.VISIBLE
         *
         */

        val escoderTeclado = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        escoderTeclado.hideSoftInputFromWindow(view.windowToken,0)
    }
}
